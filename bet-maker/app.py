from collections.abc import Sequence
from typing import TYPE_CHECKING, Any

from depends import get_session
from fastapi import Depends, FastAPI
from schema import (
    GetBetsSchemaOutput,
    GetEventsSchemaOutput,
    PostBetSchemaInput,
    PostBetSchemaOutput,
)
from servises import BetManager, EventManager
from sqlalchemy import Row

from sqlalchemy.ext.asyncio import AsyncSession
app = FastAPI()


@app.get("/events", response_model=GetEventsSchemaOutput)
async def get_events(
    session: AsyncSession = Depends(get_session),
) -> dict[str, Sequence[Row[Any]]]:
    """Получить список событий, на которые можно совершить ставку."""
    events = await EventManager(session).get_events()
    return {"row": events}


@app.post("/bet", response_model=PostBetSchemaOutput)
async def post_bet(
    data: PostBetSchemaInput, session: AsyncSession = Depends(get_session),
) -> dict[str, int | str]:
    """Совершить ставку на событие."""
    bet_id = await BetManager(session).create(data)
    return {"bet_id": bet_id, "message": "Ставка принята"}


@app.get("/bets", response_model=GetBetsSchemaOutput)
async def get_bets(
    bet_id: int | None = None, session: AsyncSession = Depends(get_session),
) -> dict[str, Sequence[Row[Any]]]:
    """Возвращает историю всех сделанных ставок."""
    bets: Sequence[Row[Any]] = await BetManager(session).get_bets(bet_id)
    return {"row": bets}
