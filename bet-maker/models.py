from decimal import Decimal

from sqlalchemy import (
    ForeignKey,
    Integer,
    Numeric,
    String,
)
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column


class Base(DeclarativeBase):
    pass


class EventStatus(Base):
    __tablename__ = "t_event_status"
    event_status_id = mapped_column(
        Integer,
        primary_key=True,
        index=True,
        autoincrement=True,
    )
    caption = mapped_column(String(200))


class Events(Base):
    __tablename__ = "t_events"
    event_id: Mapped[int] = mapped_column(
        Integer,
        primary_key=True,
        index=True,
        autoincrement=True,
    )
    caption: Mapped[str] = mapped_column(String(200), nullable=False)
    coefficient: Mapped[Decimal] = mapped_column(Numeric(15, 2), nullable=False)
    deadline: Mapped[int] = mapped_column(Integer, nullable=False)
    event_status_id: Mapped[int] = mapped_column(
        ForeignKey(EventStatus.event_status_id),
        nullable=False,
    )


class Bets(Base):
    __tablename__ = "t_bets"
    bet_id: Mapped[int] = mapped_column(
        Integer,
        primary_key=True,
        index=True,
        autoincrement=True,
    )
    event_id: Mapped[int] = mapped_column(ForeignKey(Events.event_id))
    amount: Mapped[float] = mapped_column(Numeric(15, 2), nullable=False)
