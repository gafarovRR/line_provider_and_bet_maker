from decimal import Decimal

from pydantic import BaseModel, condecimal


class MyBaseModel(BaseModel):
    class Config:
        orm_mode = True


class PostBetSchemaInput(MyBaseModel):
    event_id: int
    amount: condecimal(decimal_places=2, max_digits=15, ge=0)

    class Config:
        schema_extra = {
            "example": {
                "event_id": 1,
                "amount": 100.69,
            },
        }


class PostBetSchemaOutput(MyBaseModel):
    bet_id: int
    message: str


class GetBetsSchema(MyBaseModel):
    bet_id: int
    event_caption: str
    amount: condecimal(decimal_places=2, max_digits=15, ge=0)
    status: str


class GetBetsSchemaOutput(MyBaseModel):
    row: list[GetBetsSchema]


class GetEventsSchema(MyBaseModel):
    event_id: int
    caption: str
    coefficient: Decimal


class GetEventsSchemaOutput(MyBaseModel):
    row: list[GetEventsSchema]
