import time
from collections.abc import Sequence
from decimal import Decimal
from http import HTTPStatus
from typing import TYPE_CHECKING, Any

from const import EVENT_STATE_NEW, TIME_FOR_GET_EVENTS
from fastapi import HTTPException
from httpx import AsyncClient
from models import Bets, Events, EventStatus
from schema import PostBetSchemaInput
from settings import get_settings
from sqlalchemy import Row, Select, insert, select

from sqlalchemy.ext.asyncio import AsyncSession


class BaseManager:
    def __init__(self, session: AsyncSession):
        self.session: AsyncSession = session


class BetManager(BaseManager):
    async def create(self, data: PostBetSchemaInput) -> int:
        event_status_id: int | None = await self.session.scalar(
            select(Events.event_status_id).where(Events.event_id == data.event_id),
        )
        if event_status_id != EVENT_STATE_NEW:
            raise HTTPException(status_code=400, detail="Событие уже завершено")
        bet_id: int = await self.session.scalar(
            insert(Bets).values(dict(data)).returning(Bets.bet_id),
        )
        await self.session.commit()
        return bet_id

    async def get_bets(self, bet_id: int | None = None) -> Sequence[Row[Any]]:
        query: Select = (
            select(
                Bets.bet_id,
                Bets.amount,
                Events.caption.label("event_caption"),
                EventStatus.caption.label("status"),
            )
            .join(Events, Events.event_id == Bets.event_id)
            .join(EventStatus, EventStatus.event_status_id == Events.event_status_id)
        )
        if bet_id:
            query = query.where(Bets.bet_id == bet_id)
        execute = await self.session.execute(query)
        return execute.all()


class EventManager(BaseManager):
    async def get_events(self) -> Sequence[Row[tuple[int, str, Decimal]]]:
        result = await self.session.execute(
            select(Events.event_id, Events.caption, Events.coefficient).where(
                Events.event_status_id == EVENT_STATE_NEW,
            ),
        )
        return result.fetchall()

    async def save_many(self, data_events) -> None:
        for line in data_events:
            line["event_status_id"] = line["state"]
            event = Events(
                event_id=line["event_id"],
                caption=line["caption"],
                coefficient=line["coefficient"],
                deadline=line["deadline"],
                event_status_id=line["state"],
            )
            await self.session.merge(event)
        await self.session.commit()

    async def update_events(self, session: AsyncSession) -> None:
        settings = get_settings()
        line_provider_url = settings.LINE_PROVIDER_URl + "/events"
        async with AsyncClient() as client:
            deadline = int(time.time()) - TIME_FOR_GET_EVENTS
            result = await client.get(line_provider_url, params={"deadline": deadline})
            if result.status_code != HTTPStatus.OK:
                raise HTTPException(
                    status_code=400,
                    detail="Провайдер информации о событиях недоступен",
                )
            await EventManager(session).save_many(result.json())
