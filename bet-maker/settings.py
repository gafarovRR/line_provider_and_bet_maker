from functools import cache

from pydantic import BaseSettings


class Settings(BaseSettings):
    """Класс настроек, атрибуты автоматически подтягиваются из дотнет файла."""

    DATABASE_URL: str
    LINE_PROVIDER_URl: str
    TIME_ZONE: str = "Europe/Moscow"
    CELERY_BROKER_URL: str
    CELERY_RESULT_BACKEND: str

    class Config:
        env_file = ".env"


@cache
def get_settings():
    """Создает класс настроек и кеширует его значения."""
    return Settings()
