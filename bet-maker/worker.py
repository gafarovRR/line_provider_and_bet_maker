import asyncio
from typing import TYPE_CHECKING

from celery import Celery
from depends import async_session
from servises import EventManager
from settings import get_settings
from sqlalchemy.ext.asyncio import AsyncSession

settings = get_settings()
celery_app = Celery("tasks")

celery_app.conf.broker_url = settings.CELERY_BROKER_URL
celery_app.conf.result_backend = settings.CELERY_RESULT_BACKEND


@celery_app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    """Добавляет периодическую задачу на обновление событий в базе."""
    sender.add_periodic_task(7, update_events_task.s(), name="обновление событий")


@celery_app.task(name="Обновление событий")
def update_events_task():
    """Обновляет события в базе."""
    session: AsyncSession = async_session()
    loop = asyncio.get_event_loop()
    event_manager: EventManager = EventManager(session)
    return loop.run_until_complete(event_manager.update_events(session))
