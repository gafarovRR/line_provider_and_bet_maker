import decimal
import enum
import random
import time

from fastapi import FastAPI, HTTPException, Path
from pydantic import BaseModel


class EventState(enum.Enum):
    NEW = 1
    FINISHED_WIN = 2
    FINISHED_LOSE = 3


class Event(BaseModel):
    event_id: int
    coefficient: decimal.Decimal
    caption: str
    deadline: int
    state: EventState | None = None

    class Config:
        schema_extra = {
            "example": {
                "event_id": 1,
                "caption": "Россия - Турция",
                "coefficient": 1.5,
                "deadline": 1700346433,
                "state": 1,
            },
        }


class EventPut(BaseModel):
    event_id: int
    coefficient: decimal.Decimal | None
    caption: str | None
    deadline: int | None
    state: EventState | None


events: dict[str, Event] = {
    1: Event(
        event_id=1,
        coefficient=1.2,
        caption="Англия - Ирландия",
        deadline=int(time.time()) + 600,
        state=EventState.NEW,
    ),
    2: Event(
        event_id=2,
        coefficient=1.15,
        caption="Франция-Германия",
        deadline=int(time.time()) + 60,
        state=EventState.NEW,
    ),
    3: Event(
        event_id=3,
        coefficient=1.67,
        caption="Казахстан -Белоруссия",
        deadline=int(time.time()) + 90,
        state=EventState.NEW,
    ),
}

app = FastAPI()


@app.post("/event")
async def create_event(event: Event):
    if event.event_id not in events:
        events[event.event_id] = event
        return {}

    for p_name, p_value in event.dict(exclude_unset=True).items():
        setattr(events[event.event_id], p_name, p_value)

    return {}


@app.put("/event")
async def create_event(event: EventPut):
    if event.event_id not in events:
        raise HTTPException(status_code=400, detail="Событие не найдено")

    for p_name, p_value in event.dict(exclude_unset=True).items():
        if p_value is not None:
            setattr(events[event.event_id], p_name, p_value)

    return {}


@app.get("/event/{event_id}")
async def get_event(event_id: int = Path()):
    if event_id in events:
        return events[event_id]

    raise HTTPException(status_code=404, detail="Event not found")


def update_events_status():
    for event in events.values():
        if event.state == EventState.NEW and time.time() > event.deadline:
            event.state = random.choice(
                [EventState.FINISHED_WIN, EventState.FINISHED_LOSE],
            )


@app.get("/events")
async def get_events(deadline: int | None = None):
    update_events_status()
    if not deadline:
        deadline = time.time()
    return list(e for e in events.values() if deadline < e.deadline)
